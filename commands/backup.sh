#!/bin/bash/

#echo '# Backup Memory to SD'
#sudo rsync -aAXv /mnt/Memory --delete --exclude={"Memory/.Trash-1000/*","Memory/VM/*"} /media/nato/78C752A44A201233/

echo '# Backup Memory to Tera'
sudo rsync -asAXv --delete --max-size=1gb --exclude={"/mnt/Memory/.Trash-1000/*","/mnt/Memory/VM/*"} /mnt/Memory/ /mnt/Tera/Bak/Memory

echo '# Backup Xubuntu'
sudo rsync -asAXv --delete --max-size=1gb --exclude={"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found","swapfile"} "/" "/mnt/Tera/Bak/Xub"


echo "Backup completed"
