#!/bin/bash

cd ~/Desktop/

# Probably should check for new versions

#edit file:///etc/systemd/journald.conf


# ██.....██.██...██.██...██.██...██.
# ██.....██.███..██.██...██..██.██..
# ██.....██.██▀█▄██.██...██...███...
# ██.....██.██..███.██...██..██.██..
# ██████.██.██...██.▀█████▀.██...██.

echo '# FSearch'
sudo wget -O ~/Desktop/fsearch.deb https://github.com/cboxdoerfer/fsearch/releases/download/0.1beta1/fsearch_0.1beta1-1_amd64.deb
dpkg -i ~/Desktop/fsearch.deb

echo '# Browsh'
wget -O ~/Desktop/browsh.deb https://github.com/browsh-org/browsh/releases/download/v1.6.4/browsh_1.6.4_linux_amd64.deb
sudo dpkg -i browsh.deb

echo '# DuckDuckGo cli'
#sudo add-apt-repository -y ppa:twodopeshaggy/jarun
sudo apt-get update
sudo apt-get install -y ddgr

echo '# Stacer (Linux Manager)'
sudo add-apt-repository ppa:oguzhaninan/stacer
sudo apt-get update
sudo apt-get install -y stacer

echo '# Tag Spaces'
wget -O ~/Desktop/tagSpaces.deb https://www.tagspaces.org/downloads/tagspaces-amd64.deb
sudo dpkg -i ~/Desktop/tagSpaces.deb

echo '# Draw io'
wget -O ~/Desktop/draw_io.deb https://github.com/jgraph/drawio-desktop/releases/download/v11.2.5/draw.io-amd64-11.2.5.deb
sudo dpkg -i ~/Desktop/draw_io.deb

echo '# MySQL Workbench'
wget -O ~/Desktop/mysql-workbench.deb https://dev.mysql.com/get/Downloads/MySQLGUITools/mysql-workbench-community_8.0.15-1ubuntu18.04_amd64.deb
sudo dpkg -i ~/Desktop/mysql-workbench.deb

echo '# DBeaver'
wget -O ~/Desktop/dbeaver.tar.gz https://dbeaver.io/files/dbeaver-ce-latest-linux.gtk.x86_64.tar.gz
tar zxvf ~/Desktop/dbeaver.tar.gz
cd ~/Desktop/dbeaver
sudo cp -R dbeaver /usr/share/
cp /usr/share/dbeaver/dbeaver.desktop /usr/share/applications
sudo chmod +x -R /usr/share/dbeaver/

echo '# Discord'
cd ~/Desktop
sudo apt install libappindicator1 libc++1
wget -O discord.deb "https://discordapp.com/api/download?platform=linux&format=deb"
sudo dpkg -i ~/Desktop/discord.deb

echo '# Skype'
wget -O ~/Desktop/skype.deb https://go.skype.com/skypeforlinux-64.deb
sudo dpkg -i ~/Desktop/skype.deb
wmctrl -c Skype

echo '# Wine'
wget https://dl.winehq.org/wine-builds/Release.key
sudo apt-key add ~/Desktop/Release.key
sudo apt-add-repository 'https://dl.winehq.org/wine-builds/ubuntu/'
sudo apt install wine-stable

echo '# Birdtray'
sudo add-apt-repository ppa:linuxuprising/apps
sudo apt-get update
sudo apt install birdtray

echo '# Brave Browser'
curl https://s3-us-west-2.amazonaws.com/brave-apt/keys.asc | sudo apt-key add -
echo "deb [arch=amd64] https://s3-us-west-2.amazonaws.com/brave-apt `lsb_release -sc` main" | sudo tee -a /etc/apt/sources.list.d/brave-`lsb_release -sc`.list
sudo apt update
sudo apt install brave -y

echo '# Umple'
wget -O ~/Desktop/umple.jar http://try.umple.org/scripts/umple.jar


echo '# Dropbox'
cd ~ && wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -


echo '# midi'
sudo apt-get install timidity timidity-interfaces-extra freepats
timidity -iA

echo '# Pencil'
wget -O ~/Desktop/evolus-pencil.deb https://pencil.evolus.vn/dl/V3.0.4/Pencil_3.0.4_amd64.deb
sudo dpkg -i ~/Desktop/evolus-pencil.deb

echo '# Soulseek'
wget -O ~/Desktop/soulseek.tgz https://www.dropbox.com/s/7qh902qv2sxyp6p/SoulseekQt-2016-1-17-64bit.tgz?dl=0
sudo dpkg -i ~/Desktop/soulseek.deb

echo '# Rust'
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
sudo apt-get install -y rustc rustup



echo '# MTP for phone'
sudo add-apt-repository ppa:itachi-san/gvfs-mtp
sudo add-apt-repository ppa:webupd8team/stable
sudo apt-get update
sudo apt-get install gvfs-backends gvfs-bin gvfs-fuse gvfs-daemons -y
sudo apt-get install go-mtpfs -y

# cd mtp://[usb:002,005]/Phone/