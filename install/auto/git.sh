#!/bin/bash

# ▄████.█████.██████.██...██.█████▄.
# ██....██......██...██...██.██..██.
# ▀███▄.████....██...██...██.█████▀.
# ...██.██......██...██...██.██.....
# ████▀.█████...██...▀█████▀.██.....

echo '# Install Git'
sudo apt-get install git -y
sudo apt-get install git-extras -y
sudo apt-get install meld -y

echo "alias gitignore='mousepad .gitignore'" >> ~/.bashrc
# Configure git user
git config --global user.name CoteAnthony
git config --global user.email coteanthony0@gmail.com
git config --global alias.ant 'checkout anthony'
git config --global alias.anthony 'checkout anthony'
git config --global alias.Ant 'checkout Anthony'
git config --global alias.Anthony 'checkout Anthony'

echo '# Meld'
# Configure conflict resolution tool
git config --global merge.tool meld
git config --global diff.guitool meld
git config --global mergetool.meld.path /usr/bin/meld

echo '# Configuration'
# Streamline push to branch who don't exist remotely
git config --global push.default current
# Dont check ssl for validity
git config --global http.sslVerify false
echo '# Store remote git credentials'
git config --global credential.helper store

# ▄█████.▄█████▄.██....██.██....██.▄█████▄.██...██.▄████.
# ██▀....██▀.▀██.███▄▄███.███▄▄███.██▀.▀██.███..██.██....
# ██.....██...██.██.██.██.██.██.██.██...██.██▀█▄██.▀███▄.
# ██▄....██▄.▄██.██....██.██....██.██▄.▄██.██..███....██.
# ▀█████.▀█████▀.██....██.██....██.▀█████▀.██...██.████▀.

echo '# Update'
# Clone remote repository
git config --global alias.cl 'clone --depth 10'
# Update knowledge of remote tree
git config --global alias.f 'fetch -v'
git config --global alias.ft 'fetch -v'
git config --global alias.fet 'fetch -v'
# Pull
git config --global alias.l 'pull'
# Only pull fast forward changes
git config --global alias.ff "pull --ff-only"


echo '# Status'
# Show all aliases
git config --global alias.alias "config --get-regexp 'alias.*'"
git config --global alias.aliases "config --get-regexp 'alias.*'"
# Show stage status of current files
git config --global alias.s 'status -sb'
git config --global alias.st 'status -sb'
# Show difference between previous commit and current unstaged files
git config --global alias.d 'diff'
git config --global alias.df 'diff'
git config --global alias.dif 'diff'
# Show branchs
git config --global alias.bra 'branch -a --sort=committerdate'
# Show all branchs
git config --global alias.br "for-each-ref --sort=committerdate refs/heads/ --format='%(color:green)%(committerdate:relative)  %(color:red)%(objectname:short)%(color:reset)%(HEAD) %(color:cyan)%(refname:short)%(color:reset)  %(authorname)'"
# List Contributors with how many commit they made
git config --global alias.contrib "shortlog -n -s --no-merges"
# Get a summary of commits since yesterday (useful during standups):
git config --global alias.standup "log --reverse --branches --since=$(if [[ 'Mon' == '$(date +%a)' ]]; then echo 'friday'; else echo 'yesterday'; fi) --author=$(git config --get user.email) --format=format:'%C(cyan) %ad %Creset %s %Cgreen%d' --date=local"

echo '# Commit'
# Stage a file
git config --global alias.a 'add'
# Stage all file
git config --global alias.aa 'add *'
# Commit with an inline message
git config --global alias.c 'commit -m'
# Modify previous commit without changing the msg
git config --global alias.amend "commit --amend --no-edit"
# Modify previous commit without changing the msg with all modified files
git config --global alias.amendall "commit --all --amend --no-edit"
# Modify previous commit and change the msg
git config --global alias.amendedit "commit --amend"
# Combine add & commit
git config --global alias.aac "!echo  git add *; git commit -m"
git config --global alias.aaa "!echo  git add *; git commit --amend --no-edit"

echo '# Checkout'
# Create new branch $
git config --global alias.b 'checkout -b'
# Change to branch
git config --global alias.co 'checkout'
# Change to branch 'dev'
git config --global alias.dev 'checkout dev'
# Change to branch 'master'
git config --global alias.m 'checkout master'
git config --global alias.master 'checkout master'

echo '# Work'
# Push upstream quietly
git config --global alias.p 'push -u -q'
git config --global alias.push 'push -u -q'
# Push upstream by force
git config --global alias.pf 'push -u -q --force'
# Remove previous stashed modification
git config --global alias.pop 'stash pop'
# Merge master in this branch
git config --global alias.mm 'merge master'

echo '# Reset'
# Reset a file
git config --global alias.resetfile "checkout -- "
# Return to the state of the previous commit
git config --global alias.resethard "reset --hard HEAD"
# ???
git config --global alias.resetsoft "reset --soft HEAD"
# Delete previous commit and all changes
git config --global alias.resetcommit "reset --hard HEAD~1"
# Delete previous commit but keep changes
git config --global alias.cancelcommit "reset --soft HEAD~1"
# Revert the previous commit
git config --global alias.revertcommit "revert HEAD~1"
# Open Meld to resolve conflict
git config --global alias.mt "mergetool"
# Delete branch even if unmerged
git config --global alias.deletebranch "branch -D"

echo '# Graph'
# Show graph of commit tree
git config --global alias.g "log --all --graph --decorate --abbrev-commit --format=format:'%C(dim green)%h%C(reset) %C(bold white)%s%C(reset) %C(dim red) %an%C(reset) %C(dim blue)(%ar)%C(reset) %C(dim yellow)%d%C(reset)'"
git config --global alias.gr "log --all --graph --decorate --abbrev-commit --format=format:'%C(dim green)%h%C(reset) %C(bold white)%s%C(reset) %C(dim red) %an%C(reset) %C(dim blue)(%ar)%C(reset) %C(dim yellow)%d%C(reset)'"
git config --global alias.graph "log --all --graph --decorate --abbrev-commit --format=format:'%C(dim green)%h%C(reset) %C(bold white)%s%C(reset) %C(dim red) %an%C(reset) %C(dim blue)(%ar)%C(reset) %C(dim yellow)%d%C(reset)'"

# show Historic
git config --global alias.h "log --oneline --no-merges --pretty=format:'%C(auto)%h%Creset  %Cgreen%ad %Creset %C(bold white)%s%C(reset)  %Cblue[%an] %Cred%d' --date=format:'%Y-%m-%d  %H:%M'"
git config --global alias.hist "log --oneline --no-merges --pretty=format:'%C(auto)%h%Creset  %Cgreen%ad %Creset %C(bold white)%s%C(reset)  %Cblue[%an] %Cred%d' --date=format:'%Y-%m-%d  %H:%M'"


# █████▄.▄████▄.█████▄.█████.
# ██..██.██..██.██..██.██....
# █████..██████.█████..████..
# ██..██.██..██.██..██.██....
# ██..██.██..██.██..██.█████.


# change branch description
git config --global alias.br-desc "branch --edit-description"


# List commiter and email
git config --global alias.commiters "log --pretty='%an %ae%n%cn %ce' | sort | uniq; "

# show diff avec la branch master
git config --global alias.diffmaster "difftool --tool meld --dir-diff origin/master.."

# if you've added a tracked file to the .gitignore, it will still persist. This removes it.
git config --global alias.forget "rm --cached"

# Cherry-pick
git config --global alias.pick "cherry-pick"


# show what files are ignored
git config --global alias.ignored "ls-files --others --exclude-standard --ignored"


# remove changes and untracked files/directories
git config --global alias.purify "reset --hard; git clean -d -f;"
git config --global alias.scrub "reset --hard; git clean -d -f;"

# show the root directory of the repository
git config --global alias.pwd "rev-parse --show-toplevel"
git config --global alias.root "rev-parse --show-toplevel"


# restore a file whose deletion has been committed
#   git restore FILENAME
git config --global alias.restore 'checkout $(git rev-list -n 1 HEAD -- "$1")^ -- "$1"'
git config --global alias.revive 'checkout $(git rev-list -n 1 HEAD -- "$1")^ -- "$1"'
git config --global alias.resurrect 'checkout $(git rev-list -n 1 HEAD -- "$1")^ -- "$1"'

# remove the lock file
git config --global alias.unlock "rm -f ./.git/index.lock"

# add (`git add`) unstaged files
git config --global alias.welcome "ls-files --others --exclude-standard | xargs git add"

# #############################################################################

exit

# ██...██.██...██.██████.█████.▄████.██████.█████.█████▄.
# ██...██.███..██...██...██....██......██...██....██..██.
# ██...██.██▀█▄██...██...████..▀███▄...██...████..██..██.
# ██...██.██..███...██...██.......██...██...██....██..██.
# ▀█████▀.██...██...██...█████.████▀...██...█████.█████▀.



# show branch description
git config branch.${1:-$(git rev-parse --abbrev-ref HEAD)}.description ;

# ???
git config --global alias.hist "branch | grep -v "develop" | grep -v "release" | xargs git branch -D"

# cleans all branches locally that have already been merged.
git config --global alias.cleanBranches "branch --merged | grep -Ev '(^\*|develop)' | xargs git branch -d"

# squash
git config --global alias.squash '!f(){ git reset --soft HEAD~${1} && git commit --edit -m"$(git log --format=%B --reverse HEAD..HEAD@{1})"; };f'
git config --global alias.squashroot "rebase -i --root"

# conflict
git config --global alias.conflict?? "diff --name-only --diff-filter=U"

# Discard file ?? check out qui permet de choisir des chunks
git config --global alias.??? "checkout -p ${1};"

# "obituary" -- in which commit was a file deleted
git obit [filename]

# word diff
word-diff  = diff --word-diff
wd = diff --word-diff

