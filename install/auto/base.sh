#!/bin/bash

echo '# Login as SuperUser'
sudo su

# ▄████.██....██.▄████.██████.█████.██....██.
# ██.....██..██..██......██...██....███▄▄███.
# ▀███▄...████...▀███▄...██...████..██.██.██.
# ...██....██.......██...██...██....██....██.
# ████▀....██....████▀...██...█████.██....██.

echo '# New repository'
sudo apt-get install -y software-properties-common
sudo apt-add-repository -y ppa:teejee2008/ppa # conky manager
sudo add-apt-repository -y ppa:mmk2410/intellij-idea

echo '# Basic system update'
sudo apt update
sudo apt upgrade
sudo do-release-upgrade

echo '# Bloatware'
sudo apt remove -y gnome-mine
sudo apt remove -y gnome-sudoku
sudo apt remove -y pidgin

echo '# OS'
sudo apt-get install -y neofetch
sudo apt-get install -y xubuntu-restricted-extras
sudo apt-get install -y exfat-fuse exfat-utils

echo '# Package Manager'
sudo apt-get install -y aptitude
sudo apt-get install -y python-pip
sudo apt-get install -y npm

echo '# Interface'
sudo apt-get install -y qt5ct
sudo apt-get install -y gtk-chtheme

echo '# Panel Plugins'
sudo apt-get install -y xfce4-goodies
sudo apt-get install -y xfce4-mount-plugin
sudo apt-get install -y xfce4-sensors-plugin
sudo apt-get install -y xfce4-diskperf-plugin

# █████▄.▄████▄.▄████.█████.
# ██..██.██..██.██....██....
# █████..██████.▀███▄.████..
# ██..██.██..██....██.██....
# █████▀.██..██.████▀.█████.

echo '# Must-have'
sudo apt-get install -y vlc
sudo apt-get install -y fonts-inconsolata
sudo apt-get install -y copyq
sudo apt-get install -y synaptic
sudo apt-get install -y p7zip-full p7zip-rar

echo '# Security'
sudo apt-get install -y gtkterm
sudo apt-get install -y gufw

echo '# Privacy'
sudo apt-get install -y clamav
sudo apt-get install -y privoxy
sudo apt-get install -y keepassx
sudo apt-get install -y tor

echo '# Backup'
sudo apt-get install -y rsync
sudo apt-get install -y timeshift
sudo apt-get install -y freefilesync

echo '# File Management'
sudo apt-get install -y safe-rm
sudo apt-get install -y rar
sudo apt-get install -y fasd

echo '# Search'
sudo apt-get install -y recoll
sudo apt-get install -y fsearch


echo '# Desktop Environment'
sudo apt-get install -y redshift
sudo apt-get install -y conky-all

# ██████.▄█████▄.▄█████▄.██.....▄████.
# ..██...██▀.▀██.██▀.▀██.██.....██....
# ..██...██...██.██...██.██.....▀███▄.
# ..██...██▄.▄██.██▄.▄██.██........██.
# ..██...▀█████▀.▀█████▀.██████.████▀.

echo '# Virtual Machine'
sudo apt-get install -y virtualbox
sudo apt-get install -y docker.io

echo '# Download'
sudo apt-get install -y wget
sudo apt-get install -y curl
sudo apt-get install -y lynx
sudo apt-get install -y qbittorrent

echo '# Browser'
sudo apt-get install -y w3m
sudo apt-get install -y w3m-img
sudo apt-get install -y chromium-browser

echo '# Audio'
sudo apt-get install -y audacious
sudo apt-get install -y sox
sudo apt-get install -y audacity

echo '# Visual'
sudo apt-get install -y mplayer
sudo apt-get install -y gthumb
sudo apt-get install -y kodi

echo '# Display'
sudo apt-get install -y xdm
sudo apt-get install -y wmctrl

echo '# Language'
sudo apt-get install -y default-jre
sudo apt-get install -y python3.8
sudo apt-get install -y php7.2

echo '# IDE'
sudo apt-get install -y intellij-idea-community
sudo apt-get install -y meld
sudo apt-get install -y openjfx

echo '# Tools'
sudo apt-get install -y medit
sudo apt-get install -y units
sudo apt-get install -y rename
sudo apt-get install -y evince
sudo apt-get install -y cherrytree

echo '# Images'
sudo apt-get install -y pinta
sudo apt-get install -y shutter
sudo apt-get install -y gimp
sudo apt-get install -y libreoffice-impress
sudo apt-get install -y inkscape

echo '# Dev'
sudo apt-get install -y git
sudo apt-get install -y git-secret
sudo apt-get install -y libreoffice-draw
sudo apt-get install -y cloc

echo '# Doc'
sudo apt-get install -y zeal
sudo apt-get install -y pandoc


# ██████.█████.█████▄.██....██.██.██...██.▄████▄.██.....
# ..██...██....██..██.███▄▄███.██.███..██.██..██.██.....
# ..██...████..█████..██.██.██.██.██▀█▄██.██████.██.....
# ..██...██....██..██.██....██.██.██..███.██..██.██.....
# ..██...█████.██..██.██....██.██.██...██.██..██.██████.

echo '# Shell'
sudo apt-get install -y zsh
sudo apt-get install -y fish
sudo apt-get install -y fizsh

echo '# CLI'
sudo apt-get install -y tmux
sudo apt-get install -y neovim
sudo apt-get install -y figlet
sudo apt-get install -y pv
sudo apt-get install -y progress
sudo apt-get install -y htop

echo '# Disk Info'
sudo apt-get install -y tree
sudo apt-get install -y findmnt
sudo apt-get install -y hardinfo
sudo apt-get install -y baobab

echo '# Partition'
sudo apt-get install -y testdisk
sudo apt-get install -y gparted
sudo apt-get install -y ncdu

echo '# Compile'
sudo apt-get install -y dconf-editor
sudo apt-get install -y cmake
sudo apt-get install -y libusb-1.0-0-dev
sudo apt-get install -y python3-pyqt5

echo '# Network'
#sudo apt-get install -y samba
sudo apt-get install -y nmon
sudo apt-get install -y slurm
sudo apt-get install -y telegram-desktop

echo '# Database'
sudo apt-get install -y sqlite
sudo apt-get install -y sqlitebrowser
sudo apt-get install -y mysql-workbench
sudo apt-get install -y mysql-server


# ▄█████▄.██████.██...██.█████.█████▄.▄████.
# ██▀.▀██...██...██...██.██....██..██.██....
# ██...██...██...███████.████..█████..▀███▄.
# ██▄.▄██...██...██...██.██....██..██....██.
# ▀█████▀...██...██...██.█████.██..██.████▀.

echo '# Unsorted'

echo '# Snap'
# Make executable path
export PATH="/snap/bin:$PATH"

sudo snap install keepassxc
sudo snap install discord
sudo snap install wine-plateform
sudo snap install notepad-plus-plus

echo '# Finishing'
sudo apt autoremove -y
