#!/bin/bash

exit 1
echo '# Theme'

mkdir ~/.themes/
cd ~/.themes/

git clone https://github.com/trustable-code/Xfce-Simple-Dark.git

sudo apt-get install arc-theme -y

sudo apt-get install numix-gtk-theme -y


sudo nano /etc/profile.d/calibre.sh
export CALIBRE_USE_SYSTEM_THEME=1
echo -e '\nexport CALIBRE_USE_SYSTEM_THEME=1' >> .profile

sudo apt-get update
sudo apt-get install qt5-style-plugins -y