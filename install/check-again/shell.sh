#!/bin/bash

exit
# TODO : Must streamline this file

sudo su

# █████▄.▄█████▄.██.....██.█████.█████▄.██.....██.██...██.█████.
# ██..██.██▀.▀██.██.....██.██....██..██.██.....██.███..██.██....
# █████▀.██...██.██..█..██.████..█████..██.....██.██▀█▄██.████..
# ██.....██▄.▄██.██.███.██.██....██..██.██.....██.██..███.██....
# ██.....▀█████▀..███.███..█████.██..██.██████.██.██...██.█████.

echo '# Powerline fonts'

# Install powerline fonts
git clone https://github.com/powerline/fonts.git
cd .fonts/install.sh

# Install Inconsolata-g
git clone https://github.com/powerline/fonts ~/powerline_fonts
ln -s ~/powerline_fonts/Inconsolata-g Powerline.otf ~/.local/share/fonts/

#.███████..███████..██...██.
#....███...██.......██...██.
#...███....███████..███████.
#..███..........██..██...██.
#.███████..███████..██...██.


echo '# Zsh'
sudo apt-get install -y zsh
sudo apt-get install -y zsh-syntax-highlighting 

echo '# Zsh'
sh -c "$(wget -O- https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"


# Zsh agitnoster theme
git clone https://github.com/dbestevez/agitnoster-theme.git
cd agitnoster-theme
./install.sh
sudo apt-get install fonts-powerline -y

# Enable terminess powerline
mkdir ~/.config/fontconfig/
mkdir ~/.config/fontconfig/conf.d 
cd ~/.config/fontconfig/conf.d 
wget https://github.com/powerline/fonts/blob/master/fontconfig/50-enable-terminess-powerline.conf
fc-cache -vf
cd ~/Desktop

# Refresh fonts
sudo locale-gen en_US en_US.UTF-8
sudo dpkg-reconfigure locales


# Test unicode character
echo -e "\xE2\x98\xA0"


# Set Inconsolata-g in terminal


# ███████..██..███████..██...██..
# ██.......██..██.......██...██..
# █████....██..███████..███████..
# ██.......██.......██..██...██..
# ██.......██..███████..██...██..



echo '# Fish'
sudo apt-get install -y fish

echo '# Oh My Fish'
curl -L https://get.oh-my.fish | fish
omf install bobthefish

echo '# Set Aliases'
sudo echo "source ~/.aliases" >> /etc/fish/config.fish  


exit 1

echo '#  & Chips'
curl -Lo ~/.local/bin/chips --create-dirs https://github.com/xtendo-org/chips/releases/download/1.1.2/chips_gnulinux ; and chmod +x ~/.local/bin/chips