#!/bin/bash

# Symbolic links

# Desktop
sudo ln -s ~/Desktop /D
sudo ln -s ~/Desktop ~/D

# Memory
sudo ln -s /mnt/Memory/ /A
sudo ln -s /mnt/Memory/ ~/A
sudo ln -s /mnt/Memory/ /M
sudo ln -s /mnt/Memory/ ~/M

# Cegep
sudo ln -s /mnt/Memory/cegep/ /C
sudo ln -s /mnt/Memory/cegep/ ~/C

# Prog
sudo ln -s /mnt/Memory/prog/ /P
sudo ln -s /mnt/Memory/prog/ ~/P

# Tera
sudo ln -s /mnt/Tera/ /T
sudo ln -s /mnt/Tera/ ~/T

# www
#sudo chown nato:nato /var/www
sudo ln -s /var/www/html/ /W
sudo ln -s /var/www/html/ ~/W
sudo ln -s /var/www/ /home/nato/mnt/Memory/prog/W

# Others
sudo ln -s /mnt/Memory/prog/screenshots ~/Desktop

# Config
sudo ln -s ~/.functions /P/bash/config-files/functions
sudo ln -s ~/.aliases /P/bash/config-files/aliases
sudo ln -s ~/.profile /P/bash/config-files/profile
sudo ln -s ~/.zshrc /P/bash/config-files/zshrc
