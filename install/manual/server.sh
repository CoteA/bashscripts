exit


##
# Clock
sudo apt-get install -y ntpupdate
sudo ntpdate pool.ntp.org
sudo hwclock --systohc --utc

# Now boot to Windows and add the following registry. Just simply create a .reg file using the code below in Notepad. Save it and run it.

Windows Registry Editor Version 5.00
[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\TimeZoneInformation]
"RealTimeIsUniversal"=dword:00000001

