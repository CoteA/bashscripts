#!/bin/bash

exit

# Keepass
sudo add-apt-repository ppa:phoerious/keepassxc
sudo apt-get update
sudo apt-get install -y keepassxc

# https://www.maketecheasier.com/integrate-keepass-with-browser-in-ubuntu/
sudo apt-get install -y keepass2
cd ~/Desktop
git clone https://github.com/pfn/keepasshttp.git
sudo mkdir /usr/lib/keepass2/plugins
sudo mv ~/Desktop/keepasshttp/KeePassHttp.plgx /usr/lib/keepass2/plugins/

cd /usr/lib/keepass2
sudo mkdir Plugins
sudo wget https://passifox.appspot.com/KeePassHttp.plgx
sudo chmod 644 KeePassHttp.plgx

echo '# Install firefox extension'
echo '# Change Keepass setting to allow browser integration'
echo '# Ask extension to connect'
echo '# Share a password on both application'

exit