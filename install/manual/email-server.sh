exit

# PHP Composer
wget https://getcomposer.org/download/1.9.0/composer.phar
mv composer.phar /usr/local/bin/composer

# Sendmail
sudo apt-get install -y sendmail
sudo cp /etc/mail/sendmail.mc /etc/mail/sendmail.mc.bak
mousepad /etc/mail/sendmail.mc
# Look for the line :26 and make the changes in the sendmail.mc as below:
define(`SMART_HOST’, `smtpqf.com’)dnl 

m4 /etc/mail/sendmail.mc > /etc/mail/sendmail.cf 
/etc/init.d/sendmail start  
chkconfig sendmail on 


# Email sender (blue screen)
# https://askubuntu.com/questions/47609/how-to-have-my-php-send-mail
sudo apt-get install -y postfix
sudo apt-get install -y libsasl2-2 libsasl2-modules sasl2-bin

# Enable sasl-auth by adding these lines to /etc/postfix/main.cf
# add to /etc/postfix/main.cf
echo "smtp_sasl_auth_enable = yes" >> /etc/postfix/main.cf
echo "smtp_sasl_security_options = noplaintext noanonymous" >> /etc/postfix/main.cf
echo "smtp_sasl_password_maps = hash:/etc/postfix/sasl_password" >> /etc/postfix/main.cf

/etc/postfix/sasl_password
echo "smtp.gmail.com USERNAME@gmail.com:USERPASSWORD" > /etc/postfix/sasl_password

sudo chmod 600 /etc/postfix/sasl_password # for safety of your smtp password
sudo postmap hash:/etc/postfix/sasl_password 
sudo postmap /etc/postfix/sender_canonical
sudo /etc/init.d/postfix restart   