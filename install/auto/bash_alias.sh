#!/bin/bash

echo 'Login as SuperUser'
sudo su

echo 'uncommandable'
# alias bat='upower -i /org/freedesktop/UPower/devices/battery_BAT0| grep -E "state|to\ full|percentage"'

echo 'ALIAS'
touch ~/.aliases
echo "alias aliases='cat ~/.aliases'" >> ~/.aliases


echo '# Refresh aliases'
if [ -f ~/.aliases ]; then
    . ~/.aliases
fi

exit
