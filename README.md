# Shell scripts

This is my personnal shell repository for my personal use. Most scripts are safe but some aren't. They are just there to hold some code snippets to be used to copy/paste. Use them at your own risk! I take no responsability. Think before executing!

General Tips:
- Disable script execution on double-click on your computer
- Don't copy/paste stuff you got online to your terminal without understanding it.

*CoteA*