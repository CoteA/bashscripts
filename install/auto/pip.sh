#!/bin/bash


echo '# Python Libraries'
sudo apt-get install -y python-pip

pip install speedtest-cli
pip install pyttsx3
pip install python-gitlab
pip install requests
pip install scrapy
pip install bs4
pip install twister

# DS
pip install numpy
pip install scipy
pip install matplotlib
pip install tensorflow

pip install pygame
pip install pyQT
pip install scapy
pip install nltk
pip install IPython
pip install mysql
pip install pillow

sudo apt-get install -y python-setuptools
sudo easy_install trash-cli

