
exit

echo '# Thunderbird tray'
echo '# https://forum.manjaro.org/t/thunderbird-tray-integration-icon-with-clickable-notification-popup-and-sound/62478'


# Disable Keyring
sudo apt-get install -y seahorse
seahorse

# Configuration des application par default
sudo update-alternatives --all

# Blue screen
sudo apt-get install -y libavcodec-extra
sudo apt-get install -y wireshark

# Redshift
sudo apt-get install -y redshift-gtk
wget https://github.com/downloads/maoserr/redshiftgui/RedshiftGUI-0.2.1-Linux-x86_64.deb
# Session & Startup : AutoStart : Redshift : Edit
# Sherbrooke
redshift -o -l 45.404476:-71.888351 -t 6500:2500

# Sensors
sudo apt-get install -y lm-sensors
sudo sensors-detect

# Edit partition location
sudo mousepad /etc/fstab


# AutoTrash
sudo apt-get install -y autotrash
echo "autotrash -d 30 &" >> ~/.xsessionrc
echo "autotrash --min-free 1024" >> ~/.xsessionrc


echo '# VS Code'
echo '# Open vivaldi to download'
echo '# https://code.visualstudio.com/docs/?dv=linux64_deb '
echo '# Install it via Software'


echo '# SSH Key'
ssh-keygen -t rsa
echo '# Enter file in which to save the key (/home/youruser/.ssh/id_rsa):'


echo '# Upgrade Package Manager'
sudo snap refresh
sudo sdk upgrade


# Change default user directories
# mousepad ~/.config/user-dirs.dirs

# Auto remove sublime license popup
# https://gist.github.com/egel/b7beba6f962110596660#gistcomment-2718591



#firefox
# about:config
# browser.display.background_color


#cron job
#redshift
#backup
#
#
#fstab


# Ajouter a ~/.bashrc
source ~/.bash_profile
export PS1="$(tput bold)\[\033[32m\]\u@\h \[\033[36m\]\w\[\033[31m\]\$(parse_git_branch)\[\033[00m\] $ "

sudo apt-get install -y phpmyadmin

# https://kryogenix.org/code/pick/
# https://github.com/cgrossde/Pullover

sudo add-apt-repository ppa:oguzhaninan/stacer
sudo apt-get update
sudo apt-get install -y stacer

sudo add-apt-repository ppa:hluk/copyq
sudo apt update
sudo apt-get install -y copyq

edit file:///etc/systemd/journald.conf




echo '# dotnet'
wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb


sudo add-apt-repository universe
sudo apt-get install apt-transport-https
sudo apt-get update
sudo apt-get install dotnet-sdk-2.1


echo '# dotnet new console'
echo '# ajouter les dependance a xunit dans csproj'
echo '# dotnet restore'
echo '# ajouter _old au main dans obj/Debug/netcoreapp2.2/acceldev.Program.cs'

echo '# Ajouter au settings de Code Runner :'
echo '# "code-runner.executorMap": { "csharp": "echo '# calling dotnet run\n' && dotnet run"    }'


# Disable error reporting
sudo nano /etc/default/apport
change to 0



#############################################################################################
exit
echo 'EXIT'


echo '# Open the partition with root privileges, then right click, go to properties and change permissions.'
echo '# Give "Read & Write" permissions to Owner, Group and Others.'
sudo apt-get install -y emacs
git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d




echo '# Long file name'
touch ~/.gtkrc-2.0
echo << HEREDOC

style “xfdesktop-icon-view” {
    XfdesktopIconView::ellipsize-icon-labels = 0
}
widget_class “*XfdesktopIconView*” style “xfdesktop-icon-view”

HEREDOC >> ~/.gtkrc-2.0

