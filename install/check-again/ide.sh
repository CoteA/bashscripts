#!/bin/bash



echo '# SDK'
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"


echo '# Spring Boot CLI'
sdk install java
sdk install springboot
sdk install gradle
sdk install maven

echo '# Spacemacs'
sudo apt-get install emacs -y
git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d

echo '# New repo'

sudo apt-get install mvn -y
sudo apt-get install openjdk-8-jdk -y

echo '# umake'
sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make -y
sudo apt-get update
sudo apt-get install ubuntu-make -y

echo '# C++ compiler'
#sudo aptitude -y install gcc make zlib1g-dev

echo '# Vera Crypt'
sudo add-apt-repository ppa:unit193/encryption -y
sudo apt update
sudo apt install veracrypt -y

echo '# Double commander'
sudo add-apt-repository ppa:alexx2000/doublecmd -y
sudo apt-get update
sudo apt-get install doublecmd-qt -y

echo '# XnView'
wget https://download.xnview.com/XnViewMP-linux-x64.deb
cd ~/Desktop
sudo dpkg -i install XnViewMP-linux-x64.deb

echo '# Python'
wget https://www.python.org/ftp/python/3.6.3/Python-3.6.3.tar.xz
tar xJf Python-3.6.3.tar.xz
cd Python-3.6.3
./configure
make
make install

sudo apt install python3-distutils -y
sudo apt install python3-setuptools -y


echo '# Calibre'
sudo -v && 
  wget -nv -O- https://raw.githubusercontent.com/kovidgoyal/calibre/master/setup/linux-installer.py | 
  sudo python -c "import sys; main=lambda:sys.stderr.write('Download failed\n'); exec(sys.stdin.read()); main()"

echo '# Sublime text'
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt-get update
sudo apt-get install sublime-text


echo '# Postman'
sudo snap install postman
sudo wget --show-progress -O /usr/share/icons/postman.png https://dashboard.snapcraft.io/site_media/appmedia/2018/11/logo-mark.png
sudo exo-desktop-item-edit --create-new --type Application --name Postman --command 'postman' --icon /usr/share/icons/postman.png file:///usr/share/applications

echo '# Bleachbit'
wget -O ~/Desktop/bleachbit.deb https://www.bleachbit.org/download/file/t?file=bleachbit_2.0_all_ubuntu1710.deb
sudo dpkg -i ~/Desktop/bleachbit.deb

echo '# Vivaldi'
wget -O ~/Desktop/vivaldi.deb https://downloads.vivaldi.com/stable/vivaldi-stable_2.3.1440.61-1_amd64.deb
sudo dpkg -i ~/Desktop/vivaldi.deb

echo '# Fsearch'
wget -O ~/Desktop/fsearch.deb https://github.com/cboxdoerfer/fsearch/releases/download/0.1beta1/fsearch_0.1beta1-1_amd64.deb
sudo dpkg -i ~/Desktop/fsearch.deb

echo '# Pycharm'
sudo snap install pycharm-community --classic
./snap/pycharm-community/current/bin/pycharm.sh
echo 'Create desktop entry'

echo '# Android Studio'
sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386 -y
# https://developer.android.com/studio/
cd ~/Desktop
wget -O ~/Desktop/android-studio.zip https://dl.google.com/dl/android/studio/ide-zips/3.3.0.20/android-studio-ide-182.5199772-linux.zip
sudo unzip ~/Desktop/android-studio.zip -d /opt/

sudo wget --show-progress -O /usr/share/icons/android-studio.png  https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Android_Studio_icon.svg/1200px-Android_Studio_icon.svg.png
sudo exo-desktop-item-edit --create-new --type Application --name AndroidStudio --command './opt/android-studio/bin/studio.sh' --icon /usr/share/icons/android-studio.png file:///usr/share/applications

# Run Android Studio
./opt/android-studio/bin/studio.sh


# Mono
sudo apt install libmono-system-xml-linq4.0-cil libmono-system-data-datasetextensions4.0-cil libmono-system-runtime-serialization4.0-cil mono-mcs mono-complete -y


sudo apt-get install mono-complete -y
sudo apt-get install dotnet-sdk-2.1.4 -y

# Vectr
wget http://download.vectr.com/desktop/0.1.15/linux/Vectr-linux64.zip
unzip Vectr-linux64.zip
mv linux-unpacked vectr
cd vectr
./Vectr

# Docker GUI
sudo docker login
sudo docker run ducatel/visual-studio-linux-build-box 

# PHP Mailer
sudo apt-get install libphp-phpmailer

echo '# C#'
wget -q -O ~/Desktop/packages-microsoft-prod.deb https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb
sudo dpkg -i ~/Desktop/packages-microsoft-prod.deb
sudo add-apt-repository universe
sudo apt-get install apt-transport-https
sudo apt-get update
sudo apt-get install dotnet-sdk-2.2


