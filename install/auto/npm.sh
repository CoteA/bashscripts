#!/bin/bash


sudo apt-get install npm -y

sudo npm install -g tldr

sudo npm install -g jshint
sudo npm install -g eslint
sudo npm install -g typescript

sudo npm install -g crontab-ui
sudo npm install -g xterm
sudo npm install -g --save-dev electron
sudo npm install -g wikit