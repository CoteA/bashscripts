#!/bin/bash

exit 1

echo 'Start webserveur'
java -jar /home/backend.jar &

echo 'Stop webserveur'
fuser -k 8080/tcp

echo 'Reset webserveur'
fuser -k 8080/tcp && java -jar /home/backend.jar &

echo 'Show process list'
netstat -nltu

echo 'Teste la requete GET'
curl -G http://10.50.158.241:8080/getallorders

echo 'Windows kill
netstat -ano | findstr :8080'
taskkill /PID 999 -F 
echo 'remplace 999 par le no de process avec 8080'

echo 'Stalk a log file'
tail -f /path/thefile.log



echo 'Mount disk'
fdisk -l
mkdir /mnt/SD
mount /dev/sda4 /mnt/SD

echo '# kill process locking a file'
fuser -k filename
