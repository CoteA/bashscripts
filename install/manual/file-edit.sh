
exit

#######################################
# File edit 

# Scrollbar size
mousepad ~/.config/gtk-3.0/gtk.css

.scrollbar.horizontal slider, 
scrollbar.horizontal slider {
    min-width: 12px;
    }
.scrollbar.vertical slider, 
scrollbar.vertical slider {
    min-width: 12px;
    }

mousepad ~/.gtkrc-2.0

style "myscrollbar"
{
     GtkScrollbar::slider-width=12
}
class "GtkScrollbar" style "myscrollbar"



# Display git branch name
echo "parse_git_branch() { git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'}" >> ~/.bash_profile
source ~/.bash_profile
export PS1="$(tput bold)\[\033[32m\]\u@\h \[\033[36m\]\w\[\033[31m\]\$(parse_git_branch)\[\033[00m\] $ "


# Reload a config file
source ~/.bash_aliases

# Video download helper
wget -O download-helper.deb https://github.com/mi-g/vdhcoapp/releases/download/v1.2.4/net.downloadhelper.coapp-1.2.4-1_amd64.deb
dpkg -i download-helper.deb

# OpenAudible
wget -O openAudible.deb https://github.com/openaudible/openaudible/releases/download/v1.5.2/OpenAudible_deb_1.5.2.deb
dpkg -i openAudible.deb

########################
