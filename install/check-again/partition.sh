#!/bin/bash

exit

echo '# exFat'
sudo apt-get install exfat-fuse exfat-utils -y

echo '# boot-repair'
sudo add-apt-repository ppa:yannubuntu/boot-repair
sudo apt-get update
sudo apt-get install -y boot-repair

sudo apt-get install partitionmanager -y

echo '# find os'
sudo os-prober
sudo update-grub

echo '# tuxboot'
sudo apt-add-repository ppa:thomas.tsai/ubuntu-tuxboot
sudo apt-get update
sudo apt-get install tuxboot -y
sudo tuxboot





echo '# Repair linked shared library problem'
sudo /sbin/ldconfig -v
