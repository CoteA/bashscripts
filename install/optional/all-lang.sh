
exit

echo '# install all language'
sudo apt-get install -y afnix algol68g aplus-fsf aspectc++ aspectj \
  asymptote ats2-lang bash bc bf bison bsdgames bsh clisp clojure cmake \
  cmake coffeescript dafny dc ecere-dev elixir emacs25 erlang f2c fish \
  flex fp-compiler fsharp g++ gap gawk gcc gdb gdc \
  generator-scripting-language genius gforth gfortran ghc ghostscript \
  gnat gnu-smalltalk gnuplot gobjc golang gpt gri groff groovy guile-2.0 \
  gzip haxe icont iconx intercal iverilog jasmin-sable jq ksh libgd-dev \
  libpng-dev lisaac livescript llvm lua5.3 m4 make maxima minizinc mlton \
  mono-devel mono-mcs mono-vbnc nasm neko nickle nim node-typescript \
  nodejs ocaml octave open-cobol openjdk-8-jdk pakcs pari-gp parser3-cgi \
  perl php-cli pike8.0 python r-base rakudo ratfor rc regina-rexx ruby \
  ruby-mustache rustc scala scilab sed slsh spin squirrel3 swi-prolog \
  tcl tcsh valac vim xsltproc yabasic yorick zoem zsh