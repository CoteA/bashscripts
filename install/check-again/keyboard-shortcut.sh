
# List Custom
# xfconf-query -c xfce4-keyboard-shortcuts -p /commands/custom -l -v

# List All
# xfconf-query -c xfce4-keyboard-shortcuts -l -v

# remove
# xfconf-query -c xfce4-keyboard-shortcuts -p ‘/commands/custom/<Alt>F2’ -r -R

# Add

echo '# Xfce'
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Primary>Escape" -n -t string -s "xfce4-popup-whiskermenu"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Primary>F1"     -n -t string -s "xfce4-session-logout"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>F1"       -n -t string -s "xfce4-find-cursor"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/override"        -n -t string -s "true"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Alt>F1"         -n -t string -s "xfce4-popup-applicationsmenu"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Alt>F2"         -n -t string -s "xfrun4"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Alt>F3"         -n -t string -s "xfce4-appfinder"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Alt>F2/startup-notify" -n -t string -s "true"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Alt>F3/startup-notify" -n -t string -s "true"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Primary><Alt>Delete" -n -t string -s "xflock4"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Primary><Alt>Escape" -n -t string -s "xkill"

echo '# Utility'
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/F12" -n -t string -s "exo-open --launch TerminalEmulator --drop-down"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Primary>Print" -n -t string -s "shutter -w"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/Print"          -n -t string -s "shutter -f"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Primary><Alt>l" -n -t string -s "xflock4"

echo '# Tilde'
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>grave" -n -t string -s "gucharmap"

echo '# Launcher'
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>c" -n -t string -s "code"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>d" -n -t string -s "/usr/share/discord/Discord"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>e" -n -t string -s "mousepad"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>f" -n -t string -s "firefox about:home"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>i" -n -t string -s "intellij-idea-community"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>k" -n -t string -s "xfce4-keyboard-settings"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>m" -n -t string -s "mysql-workbench %f"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>p" -n -t string -s "xfce4-display-settings --minimal"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>r" -n -t string -s "redshift -o -l 45.404476:-71.888351 -t 6500:1000"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>s" -n -t string -s "/opt/sublime_text/sublime_text"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>t" -n -t string -s "thunar"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>u" -n -t string -s "/usr/bin/update-manager"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>v" -n -t string -s "VirtualBox %U"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>w" -n -t string -s "libreoffice --writer"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>x" -n -t string -s "libreoffice --calc"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Super>z" -n -t string -s "/mnt/Memory/prog/bash/commands/zzz.sh"

echo '# Special Keyboard'
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/XF86Calculator" -n -t string -s "mate-calc"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/XF86Display"    -n -t string -s "xfce-display-settings --minimal"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/XF86Explorer"   -n -t string -s "exo-open --launch FileManager"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/XF86HomePage"   -n -t string -s "firefox about:home"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/XF86Mail"       -n -t string -s "exo-open --launch MailReader"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/XF86Music"      -n -t string -s "parole"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/XF86WWW"        -n -t string -s "exo-open --launch WebBrowser"


echo '# Rotate screen'
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Primary><Alt>Left"  -n -t string -s "xrandr -o left"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Primary><Alt>Right" -n -t string -s "xrandr -o right"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Primary><Alt>Up"    -n -t string -s "xrandr -o normal"
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Primary><Alt>Down"  -n -t string -s "xrandr -o inverted"
