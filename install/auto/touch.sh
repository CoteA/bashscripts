#!/bin/bash


# Templates

touch ~/Templates/yml.yml
touch ~/Templates/json.json
touch ~/Templates/xml.xml
touch ~/Templates/csv.csv
echo $'Year,Make,Model\n2007,Ford,F150' > csv.csv
touch ~/Templates/ini.ini
echo $'[Settings]\n# Comment\nKey=value' > ini.ini

touch ~/Templates/py.py
touch ~/Templates/sh.sh
echo '#!/bin/bash' >> sh.sh





sudo touch /etc/init.d/on-start.sh
chmod +x /etc/init.d/on-start.sh
echo "# Change CapsLock -> Ctrl" >> /etc/init.d/on-start.sh
echo "setxkbmap -option ctrl:nocaps" >> /etc/init.d/on-start.sh
