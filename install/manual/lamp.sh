LAMP

exit 1

# https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04#how-to-find-your-server-39-s-public-ip-address

sudo apt-get install apache2 -y
sudo apt-get install php7.2 -y
sudo apt-get install mysql-server -y
sudo apt-get install tasksel -y
sudo tasksel
sudo mysql_secure_installation -y

# Download Mysql-workbench from the site, not the apt package




sudo nano /etc/apache2/mods-enabled/dir.conf
# We want to move the PHP index file highlighted above to the first position after the DirectoryIndex specification, like this:
# DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm

sudo systemctl restart apache2
sudo systemctl status apache2


sudo nano /var/www/html/info.php

# <?php phpinfo(); ?>

sudo service mysql start

sudo /etc/init.d/apache2 start
sudo /etc/init.d/apache2 restart

exit
# XAMPP

wget https://www.apachefriends.org/xampp-files/7.2.14/xampp-linux-x64-7.2.14-0-installer.run
sudo chmod 755 xampp-linux-*.run
sudo ./xampp-linux-*.run

sudo /opt/lampp/lampp start