#!/bin/bash

# Python
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get update

# Spyder
sudo apt-get install -y spyder

# Anaconda
apt-get install -y libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6
wget -O ~/Desktop/Anaconda.sh https://repo.anaconda.com/archive/Anaconda3-2019.10-Linux-x86_64.sh
sh ~/Desktop/Anaconda.sh
sudo apt-get install -y python-pip python-dev

# Jupyter Notebook
sudo apt-get -y install ipython
sudo -H pip install jupyter
sudo apt-get install -y jupyter-core

# Python3 in Jupyter Notebook
python3.8 -m pip install ipykernel
python3.8 -m ipykernel install --user

# Pip Lib
python3.8 -m pip install sqlalchemy
python3.8 -m pip install pypika
python3.8 -m pip install pyfiglet
python3.8 -m pip install cython
python3.8 -m pip install setuptools
python3.8 -m pip install wheel
python3.8 -m pip install pyarrow
python3.8 -m pip install feather-format
